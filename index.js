event.preventDefault();

function bai_1() {
  var luong_ngay = document.getElementById("luong-mot-ngay").value * 1;
  var so_ngay = document.getElementById("so-ngay").value * 1;
  var tinh_luong = luong_ngay * so_ngay;
  document.getElementById("tinh-luong").innerText = tinh_luong + "$";
}

function bai_2() {
  var so_1 = document.getElementById("so-1").value * 1;
  var so_2 = document.getElementById("so-2").value * 1;
  var so_3 = document.getElementById("so-3").value * 1;
  var so_4 = document.getElementById("so-4").value * 1;
  var so_5 = document.getElementById("so-5").value * 1;

  var tbc = (so_1 + so_2 + so_3 + so_4 + so_5) / 5;
  document.getElementById("tinh-trung-binh").innerText = tbc;
}

function bai_3() {
  var tien_USD = document.getElementById("tien-USD").value * 1;
  var ti_so_VND_USD = 23500;
  var tien_VND = tien_USD * ti_so_VND_USD;

  document.getElementById("tien_VND").innerText = new Intl.NumberFormat(
    "vn-VN",
    { style: "currency", currency: "VND" }
  ).format(tien_VND);
}

function bai_4() {
  var chieu_dai = document.getElementById("chieu-dai").value * 1;
  var chieu_rong = document.getElementById("chieu-rong").value * 1;
  var chu_vi = (chieu_dai + chieu_rong) * 2;
  var dien_tich = chieu_dai * chieu_rong;

  document.getElementById("tinh-hcn").innerText =
    "Diện tích: " + dien_tich + " - Chu vi: " + chu_vi;
}

function bai_5() {
  var so_input = document.getElementById("so-input").value * 1;
  var ky_so_2 = so_input % 10;
  var ky_so_1 = (so_input - ky_so_2) / 10;
  var tong_2_ky_so = ky_so_1 + ky_so_2;

  document.getElementById("tong-2-ky-so").innerText = tong_2_ky_so;
}
